'use strict';
const handler = require("./handler.js")

const mokcListOk = {
		"resource": "/flyers",
		"path": "/flyers",
		"httpMethod": "GET",
		"headers": {
			"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
			"Accept-Encoding": "gzip, deflate, br",
			"Accept-Language": "en-US,en;q=0.5",
			"cache-control": "no-cache",
			"CloudFront-Forwarded-Proto": "https",
			"CloudFront-Is-Desktop-Viewer": "true",
			"CloudFront-Is-Mobile-Viewer": "false",
			"CloudFront-Is-SmartTV-Viewer": "false",
			"CloudFront-Is-Tablet-Viewer": "false",
			"CloudFront-Viewer-Country": "IT",
			"Host": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
			"pragma": "no-cache",
			"upgrade-insecure-requests": "1",
			"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
			"Via": "2.0 9462251ec1005d8753d5e222d6623243.cloudfront.net (CloudFront)",
			"X-Amz-Cf-Id": "V05FelpEDzjlbnW3-JdoZKgdUXp5vprrwEchPGY1HsuPmd0nWUwDYw==",
			"X-Amzn-Trace-Id": "Root=1-5f6ccdf1-aad4ea0ed7eba09832bb028a",
			"X-Forwarded-For": "79.23.140.192, 64.252.144.89",
			"X-Forwarded-Port": "443",
			"X-Forwarded-Proto": "https"
		},
		"multiValueHeaders": {
			"Accept": [
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
			],
			"Accept-Encoding": [
				"gzip, deflate, br"
			],
			"Accept-Language": [
				"en-US,en;q=0.5"
			],
			"cache-control": [
				"no-cache"
			],
			"CloudFront-Forwarded-Proto": [
				"https"
			],
			"CloudFront-Is-Desktop-Viewer": [
				"true"
			],
			"CloudFront-Is-Mobile-Viewer": [
				"false"
			],
			"CloudFront-Is-SmartTV-Viewer": [
				"false"
			],
			"CloudFront-Is-Tablet-Viewer": [
				"false"
			],
			"CloudFront-Viewer-Country": [
				"IT"
			],
			"Host": [
				"cvty451iw8.execute-api.us-east-1.amazonaws.com"
			],
			"pragma": [
				"no-cache"
			],
			"upgrade-insecure-requests": [
				"1"
			],
			"User-Agent": [
				"Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
			],
			"Via": [
				"2.0 9462251ec1005d8753d5e222d6623243.cloudfront.net (CloudFront)"
			],
			"X-Amz-Cf-Id": [
				"V05FelpEDzjlbnW3-JdoZKgdUXp5vprrwEchPGY1HsuPmd0nWUwDYw=="
			],
			"X-Amzn-Trace-Id": [
				"Root=1-5f6ccdf1-aad4ea0ed7eba09832bb028a"
			],
			"X-Forwarded-For": [
				"79.23.140.192, 64.252.144.89"
			],
			"X-Forwarded-Port": [
				"443"
			],
			"X-Forwarded-Proto": [
				"https"
			]
		},
		"queryStringParameters": {
			"filter[category]": "Discount",
			"filter[is_published]": "1",
			"limit": "2",
			"page": "2"
		},
		"multiValueQueryStringParameters": {
			"filter[category]": [
				"Discount"
			],
			"filter[is_published]": [
				"1"
			],
			"limit": [
				"2"
			],
			"page": [
				"2"
			]
		},
		"pathParameters": null,
		"stageVariables": null,
		"requestContext": {
			"resourceId": "euin94",
			"resourcePath": "/flyers",
			"httpMethod": "GET",
			"extendedRequestId": "TYUdzGY8IAMFQPw=",
			"requestTime": "24/Sep/2020:16:48:49 +0000",
			"path": "/dev/flyers",
			"accountId": "848970116665",
			"protocol": "HTTP/1.1",
			"stage": "dev",
			"domainPrefix": "cvty451iw8",
			"requestTimeEpoch": 1600966129833,
			"requestId": "434c54ad-2c28-498a-9926-81eeef154e11",
			"identity": {
				"cognitoIdentityPoolId": null,
				"accountId": null,
				"cognitoIdentityId": null,
				"caller": null,
				"sourceIp": "79.23.140.192",
				"principalOrgId": null,
				"accessKey": null,
				"cognitoAuthenticationType": null,
				"cognitoAuthenticationProvider": null,
				"userArn": null,
				"userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
				"user": null
			},
			"domainName": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
			"apiId": "cvty451iw8"
		},
		"body": null,
		"isBase64Encoded": false
}

const mokcFlyerOk = {
	"resource": "/flyers/{id}",
	"path": "/flyers/99",
	"httpMethod": "GET",
	"headers": {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.5",
		"cache-control": "no-cache",
		"CloudFront-Forwarded-Proto": "https",
		"CloudFront-Is-Desktop-Viewer": "true",
		"CloudFront-Is-Mobile-Viewer": "false",
		"CloudFront-Is-SmartTV-Viewer": "false",
		"CloudFront-Is-Tablet-Viewer": "false",
		"CloudFront-Viewer-Country": "IT",
		"Host": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"pragma": "no-cache",
		"upgrade-insecure-requests": "1",
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
		"Via": "2.0 bc4e4c44abae4a5bb17b234953976b89.cloudfront.net (CloudFront)",
		"X-Amz-Cf-Id": "NvdzdQKIwu_4NrMyy3wTbQzHOXqexS0sLu84xfNTHudhlbBzrSql4Q==",
		"X-Amzn-Trace-Id": "Root=1-5f6cd181-50160f34d4aa3654d5021a82",
		"X-Forwarded-For": "79.23.140.192, 64.252.144.79",
		"X-Forwarded-Port": "443",
		"X-Forwarded-Proto": "https"
	},
	"multiValueHeaders": {
		"Accept": [
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		],
		"Accept-Encoding": [
			"gzip, deflate, br"
		],
		"Accept-Language": [
			"en-US,en;q=0.5"
		],
		"cache-control": [
			"no-cache"
		],
		"CloudFront-Forwarded-Proto": [
			"https"
		],
		"CloudFront-Is-Desktop-Viewer": [
			"true"
		],
		"CloudFront-Is-Mobile-Viewer": [
			"false"
		],
		"CloudFront-Is-SmartTV-Viewer": [
			"false"
		],
		"CloudFront-Is-Tablet-Viewer": [
			"false"
		],
		"CloudFront-Viewer-Country": [
			"IT"
		],
		"Host": [
			"cvty451iw8.execute-api.us-east-1.amazonaws.com"
		],
		"pragma": [
			"no-cache"
		],
		"upgrade-insecure-requests": [
			"1"
		],
		"User-Agent": [
			"Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
		],
		"Via": [
			"2.0 bc4e4c44abae4a5bb17b234953976b89.cloudfront.net (CloudFront)"
		],
		"X-Amz-Cf-Id": [
			"NvdzdQKIwu_4NrMyy3wTbQzHOXqexS0sLu84xfNTHudhlbBzrSql4Q=="
		],
		"X-Amzn-Trace-Id": [
			"Root=1-5f6cd181-50160f34d4aa3654d5021a82"
		],
		"X-Forwarded-For": [
			"79.23.140.192, 64.252.144.79"
		],
		"X-Forwarded-Port": [
			"443"
		],
		"X-Forwarded-Proto": [
			"https"
		]
	},
	"queryStringParameters": {
		"fields": "id,category"
	},
	"multiValueQueryStringParameters": {
		"fields": [
			"id,category"
		]
	},
	"pathParameters": {
		"id": "99"
	},
	"stageVariables": null,
	"requestContext": {
		"resourceId": "4lh21s",
		"resourcePath": "/flyers/{id}",
		"httpMethod": "GET",
		"extendedRequestId": "TYWsPG7xoAMF5rQ=",
		"requestTime": "24/Sep/2020:17:04:01 +0000",
		"path": "/dev/flyers/99",
		"accountId": "848970116665",
		"protocol": "HTTP/1.1",
		"stage": "dev",
		"domainPrefix": "cvty451iw8",
		"requestTimeEpoch": 1600967041495,
		"requestId": "c68785bf-ecfa-4b60-9ff5-c359cf3beff7",
		"identity": {
			"cognitoIdentityPoolId": null,
			"accountId": null,
			"cognitoIdentityId": null,
			"caller": null,
			"sourceIp": "79.23.140.192",
			"principalOrgId": null,
			"accessKey": null,
			"cognitoAuthenticationType": null,
			"cognitoAuthenticationProvider": null,
			"userArn": null,
			"userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
			"user": null
		},
		"domainName": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"apiId": "cvty451iw8"
	},
	"body": null,
	"isBase64Encoded": false
}

const mokcListOutOfBound = {
	"resource": "/flyers",
	"path": "/flyers",
	"httpMethod": "GET",
	"headers": {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.5",
		"cache-control": "no-cache",
		"CloudFront-Forwarded-Proto": "https",
		"CloudFront-Is-Desktop-Viewer": "true",
		"CloudFront-Is-Mobile-Viewer": "false",
		"CloudFront-Is-SmartTV-Viewer": "false",
		"CloudFront-Is-Tablet-Viewer": "false",
		"CloudFront-Viewer-Country": "IT",
		"Host": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"pragma": "no-cache",
		"upgrade-insecure-requests": "1",
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
		"Via": "2.0 10eb694085881f80602b0213448c7131.cloudfront.net (CloudFront)",
		"X-Amz-Cf-Id": "Vh3QXF5a8V5zoXecN0pE6vkRcBrzbEv-UIWSK5cc83dzPg2nCO74Mg==",
		"X-Amzn-Trace-Id": "Root=1-5f6cceae-0d3c36bb03382a5d343d6036",
		"X-Forwarded-For": "79.23.140.192, 64.252.144.142",
		"X-Forwarded-Port": "443",
		"X-Forwarded-Proto": "https"
	},
	"multiValueHeaders": {
		"Accept": [
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		],
		"Accept-Encoding": [
			"gzip, deflate, br"
		],
		"Accept-Language": [
			"en-US,en;q=0.5"
		],
		"cache-control": [
			"no-cache"
		],
		"CloudFront-Forwarded-Proto": [
			"https"
		],
		"CloudFront-Is-Desktop-Viewer": [
			"true"
		],
		"CloudFront-Is-Mobile-Viewer": [
			"false"
		],
		"CloudFront-Is-SmartTV-Viewer": [
			"false"
		],
		"CloudFront-Is-Tablet-Viewer": [
			"false"
		],
		"CloudFront-Viewer-Country": [
			"IT"
		],
		"Host": [
			"cvty451iw8.execute-api.us-east-1.amazonaws.com"
		],
		"pragma": [
			"no-cache"
		],
		"upgrade-insecure-requests": [
			"1"
		],
		"User-Agent": [
			"Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
		],
		"Via": [
			"2.0 10eb694085881f80602b0213448c7131.cloudfront.net (CloudFront)"
		],
		"X-Amz-Cf-Id": [
			"Vh3QXF5a8V5zoXecN0pE6vkRcBrzbEv-UIWSK5cc83dzPg2nCO74Mg=="
		],
		"X-Amzn-Trace-Id": [
			"Root=1-5f6cceae-0d3c36bb03382a5d343d6036"
		],
		"X-Forwarded-For": [
			"79.23.140.192, 64.252.144.142"
		],
		"X-Forwarded-Port": [
			"443"
		],
		"X-Forwarded-Proto": [
			"https"
		]
	},
	"queryStringParameters": {
		"filter[category]": "Discount",
		"filter[is_published]": "1",
		"limit": "200",
		"page": "200"
	},
	"multiValueQueryStringParameters": {
		"filter[category]": [
			"Discount"
		],
		"filter[is_published]": [
			"1"
		],
		"limit": [
			"200"
		],
		"page": [
			"200"
		]
	},
	"pathParameters": null,
	"stageVariables": null,
	"requestContext": {
		"resourceId": "euin94",
		"resourcePath": "/flyers",
		"httpMethod": "GET",
		"extendedRequestId": "TYU7PE_sIAMF7qw=",
		"requestTime": "24/Sep/2020:16:51:58 +0000",
		"path": "/dev/flyers",
		"accountId": "848970116665",
		"protocol": "HTTP/1.1",
		"stage": "dev",
		"domainPrefix": "cvty451iw8",
		"requestTimeEpoch": 1600966318216,
		"requestId": "cf54ef0b-91a3-43b1-8b4c-75d319db33b2",
		"identity": {
			"cognitoIdentityPoolId": null,
			"accountId": null,
			"cognitoIdentityId": null,
			"caller": null,
			"sourceIp": "79.23.140.192",
			"principalOrgId": null,
			"accessKey": null,
			"cognitoAuthenticationType": null,
			"cognitoAuthenticationProvider": null,
			"userArn": null,
			"userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
			"user": null
		},
		"domainName": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"apiId": "cvty451iw8"
	},
	"body": null,
	"isBase64Encoded": false
}

const mokcFlyerOutOfBound = {
	"resource": "/flyers/{id}",
	"path": "/flyers/999",
	"httpMethod": "GET",
	"headers": {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.5",
		"cache-control": "no-cache",
		"CloudFront-Forwarded-Proto": "https",
		"CloudFront-Is-Desktop-Viewer": "true",
		"CloudFront-Is-Mobile-Viewer": "false",
		"CloudFront-Is-SmartTV-Viewer": "false",
		"CloudFront-Is-Tablet-Viewer": "false",
		"CloudFront-Viewer-Country": "IT",
		"Host": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"pragma": "no-cache",
		"upgrade-insecure-requests": "1",
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
		"Via": "2.0 eaaa1e97697a6ab196c5224bbc70d9c8.cloudfront.net (CloudFront)",
		"X-Amz-Cf-Id": "v9XKZhDcTA_4C1Ym7KuBctGJhC9y9ttEBd8nrjW1iRnaIRess_CYWw==",
		"X-Amzn-Trace-Id": "Root=1-5f6cd026-1a3b6729f287b8b10aa8c019",
		"X-Forwarded-For": "79.23.140.192, 64.252.144.72",
		"X-Forwarded-Port": "443",
		"X-Forwarded-Proto": "https"
	},
	"multiValueHeaders": {
		"Accept": [
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		],
		"Accept-Encoding": [
			"gzip, deflate, br"
		],
		"Accept-Language": [
			"en-US,en;q=0.5"
		],
		"cache-control": [
			"no-cache"
		],
		"CloudFront-Forwarded-Proto": [
			"https"
		],
		"CloudFront-Is-Desktop-Viewer": [
			"true"
		],
		"CloudFront-Is-Mobile-Viewer": [
			"false"
		],
		"CloudFront-Is-SmartTV-Viewer": [
			"false"
		],
		"CloudFront-Is-Tablet-Viewer": [
			"false"
		],
		"CloudFront-Viewer-Country": [
			"IT"
		],
		"Host": [
			"cvty451iw8.execute-api.us-east-1.amazonaws.com"
		],
		"pragma": [
			"no-cache"
		],
		"upgrade-insecure-requests": [
			"1"
		],
		"User-Agent": [
			"Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
		],
		"Via": [
			"2.0 eaaa1e97697a6ab196c5224bbc70d9c8.cloudfront.net (CloudFront)"
		],
		"X-Amz-Cf-Id": [
			"v9XKZhDcTA_4C1Ym7KuBctGJhC9y9ttEBd8nrjW1iRnaIRess_CYWw=="
		],
		"X-Amzn-Trace-Id": [
			"Root=1-5f6cd026-1a3b6729f287b8b10aa8c019"
		],
		"X-Forwarded-For": [
			"79.23.140.192, 64.252.144.72"
		],
		"X-Forwarded-Port": [
			"443"
		],
		"X-Forwarded-Proto": [
			"https"
		]
	},
	"queryStringParameters": null,
	"multiValueQueryStringParameters": null,
	"pathParameters": {
		"id": "999"
	},
	"stageVariables": null,
	"requestContext": {
		"resourceId": "4lh21s",
		"resourcePath": "/flyers/{id}",
		"httpMethod": "GET",
		"extendedRequestId": "TYV2EFB9oAMF4vQ=",
		"requestTime": "24/Sep/2020:16:58:14 +0000",
		"path": "/dev/flyers/999",
		"accountId": "848970116665",
		"protocol": "HTTP/1.1",
		"stage": "dev",
		"domainPrefix": "cvty451iw8",
		"requestTimeEpoch": 1600966694728,
		"requestId": "5859fc18-746f-4261-9f4e-aadbf27b63d8",
		"identity": {
			"cognitoIdentityPoolId": null,
			"accountId": null,
			"cognitoIdentityId": null,
			"caller": null,
			"sourceIp": "79.23.140.192",
			"principalOrgId": null,
			"accessKey": null,
			"cognitoAuthenticationType": null,
			"cognitoAuthenticationProvider": null,
			"userArn": null,
			"userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
			"user": null
		},
		"domainName": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"apiId": "cvty451iw8"
	},
	"body": null,
	"isBase64Encoded": false
}

const mokcListWrongFields = {
	"resource": "/flyers",
	"path": "/flyers",
	"httpMethod": "GET",
	"headers": {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.5",
		"cache-control": "no-cache",
		"CloudFront-Forwarded-Proto": "https",
		"CloudFront-Is-Desktop-Viewer": "true",
		"CloudFront-Is-Mobile-Viewer": "false",
		"CloudFront-Is-SmartTV-Viewer": "false",
		"CloudFront-Is-Tablet-Viewer": "false",
		"CloudFront-Viewer-Country": "IT",
		"Host": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"pragma": "no-cache",
		"upgrade-insecure-requests": "1",
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
		"Via": "2.0 bc4e4c44abae4a5bb17b234953976b89.cloudfront.net (CloudFront)",
		"X-Amz-Cf-Id": "lrd7qJPZq_W0NxvEOkc6sRop5a4EFgotDpnIMkwm0yLvcm-u4JS4xQ==",
		"X-Amzn-Trace-Id": "Root=1-5f6cd0dd-0b69ff58eec1ef1a8a9b1d7e",
		"X-Forwarded-For": "79.23.140.192, 64.252.144.91",
		"X-Forwarded-Port": "443",
		"X-Forwarded-Proto": "https"
	},
	"multiValueHeaders": {
		"Accept": [
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		],
		"Accept-Encoding": [
			"gzip, deflate, br"
		],
		"Accept-Language": [
			"en-US,en;q=0.5"
		],
		"cache-control": [
			"no-cache"
		],
		"CloudFront-Forwarded-Proto": [
			"https"
		],
		"CloudFront-Is-Desktop-Viewer": [
			"true"
		],
		"CloudFront-Is-Mobile-Viewer": [
			"false"
		],
		"CloudFront-Is-SmartTV-Viewer": [
			"false"
		],
		"CloudFront-Is-Tablet-Viewer": [
			"false"
		],
		"CloudFront-Viewer-Country": [
			"IT"
		],
		"Host": [
			"cvty451iw8.execute-api.us-east-1.amazonaws.com"
		],
		"pragma": [
			"no-cache"
		],
		"upgrade-insecure-requests": [
			"1"
		],
		"User-Agent": [
			"Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
		],
		"Via": [
			"2.0 bc4e4c44abae4a5bb17b234953976b89.cloudfront.net (CloudFront)"
		],
		"X-Amz-Cf-Id": [
			"lrd7qJPZq_W0NxvEOkc6sRop5a4EFgotDpnIMkwm0yLvcm-u4JS4xQ=="
		],
		"X-Amzn-Trace-Id": [
			"Root=1-5f6cd0dd-0b69ff58eec1ef1a8a9b1d7e"
		],
		"X-Forwarded-For": [
			"79.23.140.192, 64.252.144.91"
		],
		"X-Forwarded-Port": [
			"443"
		],
		"X-Forwarded-Proto": [
			"https"
		]
	},
	"queryStringParameters": {
		"filter[categoryz]": "Discount",
		"filter[is_published]": "1",
		"limit": "2",
		"page": "20"
	},
	"multiValueQueryStringParameters": {
		"filter[categoryz]": [
			"Discount"
		],
		"filter[is_published]": [
			"1"
		],
		"limit": [
			"2"
		],
		"page": [
			"20"
		]
	},
	"pathParameters": null,
	"stageVariables": null,
	"requestContext": {
		"resourceId": "euin94",
		"resourcePath": "/flyers",
		"httpMethod": "GET",
		"extendedRequestId": "TYWSpGwJoAMF3uA=",
		"requestTime": "24/Sep/2020:17:01:17 +0000",
		"path": "/dev/flyers",
		"accountId": "848970116665",
		"protocol": "HTTP/1.1",
		"stage": "dev",
		"domainPrefix": "cvty451iw8",
		"requestTimeEpoch": 1600966877655,
		"requestId": "904fc4ef-b504-4b0a-a562-d83a373b7e29",
		"identity": {
			"cognitoIdentityPoolId": null,
			"accountId": null,
			"cognitoIdentityId": null,
			"caller": null,
			"sourceIp": "79.23.140.192",
			"principalOrgId": null,
			"accessKey": null,
			"cognitoAuthenticationType": null,
			"cognitoAuthenticationProvider": null,
			"userArn": null,
			"userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
			"user": null
		},
		"domainName": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"apiId": "cvty451iw8"
	},
	"body": null,
	"isBase64Encoded": false
}

const mokcFlyerWrongFields = {
	"resource": "/flyers/{id}",
	"path": "/flyers/99",
	"httpMethod": "GET",
	"headers": {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.5",
		"cache-control": "no-cache",
		"CloudFront-Forwarded-Proto": "https",
		"CloudFront-Is-Desktop-Viewer": "true",
		"CloudFront-Is-Mobile-Viewer": "false",
		"CloudFront-Is-SmartTV-Viewer": "false",
		"CloudFront-Is-Tablet-Viewer": "false",
		"CloudFront-Viewer-Country": "IT",
		"Host": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"pragma": "no-cache",
		"upgrade-insecure-requests": "1",
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
		"Via": "2.0 bc4e4c44abae4a5bb17b234953976b89.cloudfront.net (CloudFront)",
		"X-Amz-Cf-Id": "4mZT25MC2AL08AuNu7LRJaF6R8RePAjsHp1-4zM45woOQGBcFoVz9w==",
		"X-Amzn-Trace-Id": "Root=1-5f6cd1f3-575740d0253299704d9e6640",
		"X-Forwarded-For": "79.23.140.192, 64.252.144.90",
		"X-Forwarded-Port": "443",
		"X-Forwarded-Proto": "https"
	},
	"multiValueHeaders": {
		"Accept": [
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		],
		"Accept-Encoding": [
			"gzip, deflate, br"
		],
		"Accept-Language": [
			"en-US,en;q=0.5"
		],
		"cache-control": [
			"no-cache"
		],
		"CloudFront-Forwarded-Proto": [
			"https"
		],
		"CloudFront-Is-Desktop-Viewer": [
			"true"
		],
		"CloudFront-Is-Mobile-Viewer": [
			"false"
		],
		"CloudFront-Is-SmartTV-Viewer": [
			"false"
		],
		"CloudFront-Is-Tablet-Viewer": [
			"false"
		],
		"CloudFront-Viewer-Country": [
			"IT"
		],
		"Host": [
			"cvty451iw8.execute-api.us-east-1.amazonaws.com"
		],
		"pragma": [
			"no-cache"
		],
		"upgrade-insecure-requests": [
			"1"
		],
		"User-Agent": [
			"Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
		],
		"Via": [
			"2.0 bc4e4c44abae4a5bb17b234953976b89.cloudfront.net (CloudFront)"
		],
		"X-Amz-Cf-Id": [
			"4mZT25MC2AL08AuNu7LRJaF6R8RePAjsHp1-4zM45woOQGBcFoVz9w=="
		],
		"X-Amzn-Trace-Id": [
			"Root=1-5f6cd1f3-575740d0253299704d9e6640"
		],
		"X-Forwarded-For": [
			"79.23.140.192, 64.252.144.90"
		],
		"X-Forwarded-Port": [
			"443"
		],
		"X-Forwarded-Proto": [
			"https"
		]
	},
	"queryStringParameters": {
		"fields": "id,categoryz"
	},
	"multiValueQueryStringParameters": {
		"fields": [
			"id,categoryz"
		]
	},
	"pathParameters": {
		"id": "99"
	},
	"stageVariables": null,
	"requestContext": {
		"resourceId": "4lh21s",
		"resourcePath": "/flyers/{id}",
		"httpMethod": "GET",
		"extendedRequestId": "TYW-DGJroAMFTrQ=",
		"requestTime": "24/Sep/2020:17:05:55 +0000",
		"path": "/dev/flyers/99",
		"accountId": "848970116665",
		"protocol": "HTTP/1.1",
		"stage": "dev",
		"domainPrefix": "cvty451iw8",
		"requestTimeEpoch": 1600967155484,
		"requestId": "e9f0574b-6571-4fec-9718-e033a4cab9ce",
		"identity": {
			"cognitoIdentityPoolId": null,
			"accountId": null,
			"cognitoIdentityId": null,
			"caller": null,
			"sourceIp": "79.23.140.192",
			"principalOrgId": null,
			"accessKey": null,
			"cognitoAuthenticationType": null,
			"cognitoAuthenticationProvider": null,
			"userArn": null,
			"userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
			"user": null
		},
		"domainName": "cvty451iw8.execute-api.us-east-1.amazonaws.com",
		"apiId": "cvty451iw8"
	},
	"body": null,
	"isBase64Encoded": false
}

void async function(){
	test(
		"multiple get",
		async () => {
			var service = await handler.flyer(mokcFlyerOk)
			assert(JSON.parse(service.body).id == 99)

			service = await handler.list(mokcListOk)
			assert(JSON.parse(service.body).length == 2)

			service = await handler.flyer(mokcFlyerOk)
			assert(JSON.parse(service.body).id == 99)
		}
	)

	test(
		"out of bound",
		async () => {
			var service = await handler.list(mokcListOutOfBound)
			assert(service.statusCode == 404)

			service = await handler.flyer(mokcFlyerOutOfBound)
			assert(service.statusCode == 404)
		}
	)

	test(
		"illegal column",
		async () => {
			service = await handler.flyer(mokcFlyerWrongFields)
			assert(service.statusCode == 400)

			var service = await handler.list(mokcListWrongFields)
			assert(service.statusCode == 400)
		}
	)

	async function test(description, handler){
		try {
			await handler()
			console.log(description, 'OK')
		} catch(e) {
			console.error(description, 'KO', e)
		}
	}

	function assert(p){
		if(!p){
			debugger
			throw p
		}
	}
}()