'use strict';

void function(exports){
	exports.Flyer = class Flyer {
		constructor(o){
			this.data = o;
		}

		print(col = Object.keys(this.data)){
			var output = {}
			for(var i in col){
				if(col[i] in this.data){
					output[col[i]] = this.data[col[i]]
				} else {
					throw new exports.IllegalColumn("illegal field")
				}
			}
			return output
		}
	}

	exports.Builder = class {
		constructor(parser, type) {
			this.parser = parser
			this.type = type
		}

		find(verify = () => true){
			do {
				var record = this.parser.read()
				const data = record && new this.type(record, this.columns)
				if(data && verify(data)) {
					return data
				} 
			} while (record)
			throw new exports.OutOfBound("Out of bound")
		}

		printAll(handler, page = 1, limit = 100, output = []){
			try {
				let counter = 0
				do {
					var current = this.find(handler)
					if(counter >= page * limit) {
						break
					} else if(counter >= (page - 1) * limit){
						output.push(current.print())
					}
					counter++
				} while (current)
			} catch (e) {
				if(!output.length){
					throw e
				}
			}
			return output;
		}
	}

	exports.Service = class {
		constructor(builder){
			this.builder = builder
		}

		list({filters, page, limit}, output) {
			return this.builder.printAll(
				makeFieldFilter(filters), page, limit, output
			)
		}

		flyer({id, fields}) {
			return this.builder.find(makeFieldFilter({id})).print(
				fields ? fields.split(',') : undefined
			)
		}
	}

	function makeFieldFilter(filters) {
		return filters ? x => {
			for(var i in filters){
				if(x.data[i] != filters[i]){
					return false
				}
			}
			return true
		} : undefined
	}

	exports.OutOfBound = class extends Error {
		code = 404
	}

	exports.IllegalColumn = class extends Error {
		code = 400
	}
}(module.exports)