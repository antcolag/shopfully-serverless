'use strict';
void function({
	exports = module.exports,
	path = "./flyers_data.csv",
	input = require("fs"),
	streamBuilder = {
		stream: null,
		build() {
			return this.stream = input.createReadStream(path)
		},
		close() {
			this.stream.close()
		}
	},
	libCsv = require('csv-parser'),
	libFlyer = require("./parser.js"),
	validFilters = ["category", "is_published"]
}){
	exports.list = safeServe(async function (event, inputStream) {
		return await buildResponseBody({
			...event.queryStringParameters,
			filters: extractFilters(event.queryStringParameters)
		}, libFlyer.Service.prototype.list, inputStream)
	});

	exports.flyer = safeServe(async function (event, inputStream) {
		return await buildResponseBody({
			...event.queryStringParameters,
			...event.pathParameters
		}, libFlyer.Service.prototype.flyer, inputStream)
	});

	async function buildResponseBody(event, handler, inputStream) {
		return handler.call(await initService(inputStream), event)
	}

	function initService(stream) {
		return buildFlyerService(
			stream,
			libCsv()
		)
	}

	async function buildFlyerService(stream, parser) {
		return new libFlyer.Service (
			new libFlyer.Builder (
				await makeStreamParser(stream, parser),
				libFlyer.Flyer
			)
		)
	}

	async function makeStreamParser(stream, parser) {
		stream.pipe(parser)
		await new Promise(resolve => {
			parser.on('readable', function(){
				resolve()
			})
		})
		return parser
	}

	function safeServe(handler){
		return async function (evt, ...tail) {
			const inputStream = streamBuilder.build()
			try {
				return new Response(200, JSON.stringify(
					await handler.call(this, evt, inputStream, ...tail))
				)
			} catch (e) {
				return new Response(e.code || 500, JSON.stringify({
					message: e.message
				}))
			} finally {
				inputStream.close();
			}
		}
	}

	function extractFilters(o = {}) {
		const output = {}
		for(let i in o){
			let filter = i.match(/filter\[(.+)\]/)
			if(filter){
				if(validFilters.indexOf(filter[1]) >= 0){
					output[filter[1]] = o[i]
				} else throw new libFlyer.IllegalColumn("illegal filters")
			}
		}
		return output || undefined
	}

	class Response {
		constructor(statusCode, body){
			this.statusCode = statusCode
			this.body = body
		}
	}
}(function(){
	try {
		return require('./conf.js');
	} catch {
		return {}
	}
}())