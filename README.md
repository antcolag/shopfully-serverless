Flyer api
===

This Serverless project can serve the data in this [csv file](./shopfully/flyer_data.csv), following this [specs](./2020_Istruzioni_TestCase_BackEndDevMarketplace.pdf)

Instructions
---
- Installation
	- clone the repo or download and unzip the project
	- 
		```
		npm i
		```

- Deploy the service
	```
	npm start
	```

- Stop the service
	```
	npm stop
	```

- Run tests
	```
	npm tests
	```

Api
---

- ```/flyers/{{id}}```: returns a single flyer
	- **fields** *int* default ```null```: field to show

- ```/flyers```: returns the list of flyers, accepts the following GET arguments
	- **page** *int* default ```1```: page number
	- **limit** *int* default ```100```: page size
	- **filter** *object* default ```null```: filter by values

the api interface is decribed more formally in the [openapi.yaml](./openapi.yaml)

Resources
---
- [flyers_data.csv](./shopfully/flyers_data.csv): datasource
- [2020_Istruzioni_TestCase_BackEndDevMarketplace.pdf](./2020_Istruzioni_TestCase_BackEndDevMarketplace.pdf): specs
- [docker-compose.yml](./docker-compose.yml): builds environment
- [openapi.yaml](./openapi.yaml): OpenApi 3.0.0 spec

Note
---
The logic is handled in the [parser.js](./shopfully/parser.js) file, the requests are served in the [handler.js](./shopfully/handler.js) file.

The environment specific aguments for the handler.js script can be edited creating a conf.js file in the shopfully directory
ie
```
// shopfully/conf.js
module.export = {
	path: '/path/to/another.file'
}
```